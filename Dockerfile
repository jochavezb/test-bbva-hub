FROM registry.access.redhat.com/rhel7/rhel-tools:7.6-8
ENV BAR=BAR_CORE.bar espera=100 LogApp=/app/core.log IsAuthBase=false AdmSec=inactive IsSnapshot=inactive OutFormSna=bluemix  IsRepRes=true OutFormRes=bluemix
ENV RUTA_ACE_DB=/opt/IBM/ace-11.0.0.3/server/ODBC/unixodbc ODBCINI=$RUTA_ACE_DB/odbc.ini  ODBCSYSINI=$RUTA_ACE_DB/odbcinst.ini IsLogCon=false Format=text
ENV NameQmgr=

USER root
COPY ace11.tar.gz ini.sh 9.0.5.0-IBM-MQC-LinuxX64.tar.gz telnet-0.17-64.el7.x86_64.rpm /
COPY 9.0.5.0-IBM-MQC-LinuxX64.tar.gz /opt/

RUN  yum install -y telnet-0.17-64.el7.x86_64.rpm
RUN  mkdir -p /opt/IBM && mkdir -p /var/mqsi && groupadd mqbrkrs && useradd -g mqbrkrs -G mqbrkrs -d /home/aceadmin -m -s /bin/bash aceadmin && mkdir /home/aceadmin/bar  && tar -zxvf ace11.tar.gz --exclude ace-11.0.0.3/tools -C /opt/IBM/ && tar -zxvf 9.0.5.0-IBM-MQC-LinuxX64.tar.gz -C /opt/ && chown -R aceadmin:mqbrkrs /opt/IBM/ace-11.0.0.3 && chown -R aceadmin:mqbrkrs /var/mqsi && chmod -R 775 /opt/IBM/ace-11.0.0.3 && chmod -R 775 /var/mqsi && chmod 777 /ini.sh  && mkdir -p /app/bbva/esb/logsApp && mkdir -p /app/bbva/esb/logsExec && chown -R aceadmin:mqbrkrs /app && mkdir -p /opt/IBM/ace-11.0.0.3/server/ODBC/drivers/lib && chmod -R 775  /app &&  chmod -R 775 /opt/IBM/ace-11.0.0.3/server/ODBC && chown -R aceadmin:mqbrkrs /opt/IBM/ace-11.0.0.3/server/ODBC && chown -R aceadmin:mqbrkrs /home/aceadmin && chmod -R 775 /home/aceadmin && mkdir -p /opt/mqm/lib64 && groupadd mqm && useradd -g mqm mqm && chown -R mqm:mqm /opt/mqm && chmod -R 775 /opt/mqm &&  rm -f /ace11.tar.gz &&  rm -f 9.0.5.0-IBM-MQC-LinuxX64.tar.gz  && rm -f *.rpm
#RUN chmod 777 /opt/MQSeriesClient-9.0.4-0.x86_64.rpm && chown mqm:mqm /opt/MQSeriesClient-9.0.4-0.x86_64.rpm && chmod 777 /opt/MQSeriesRuntime-9.0.4-0.x86_64.rpm && chown mqm:mqm /opt/MQSeriesRuntime-9.0.4-0.x86_64.rpm && chmod 777 /opt/mqlicense.sh && chown mqm:mqm /opt/mqlicense.sh
#Excluyendo el toolkit
RUN cd /opt && ./mqlicense.sh -accept && rpm --prefix /opt/mqm -ivh MQSeries* && cd /opt/mqm/bin && ./setmqinst -i -p /opt/mqm
COPY /lib/* /opt/mqm/lib64/
COPY libUKicu95.so libUKssl95.so UKase95.so UKasedtc95.so UKora95.so UKoradtc95.so UKsqls95.so UKtrc95.so /opt/IBM/ace-11.0.0.3/server/ODBC/drivers/lib/
COPY odbc.ini odbcinst.ini $RUTA_ACE_DB/
COPY $BAR /home/aceadmin/bar/
RUN chmod -R 775 /opt/IBM/ace-11.0.0.3/server/ODBC && chown -R aceadmin:mqbrkrs /opt/IBM/ace-11.0.0.3/server/ODBC && chown -R aceadmin:mqbrkrs /home/aceadmin && chmod -R 775 /home/aceadmin && chown -R mqm:mqm /opt/mqm && chmod -R 775 /opt/mqm
USER aceadmin
RUN  /opt/IBM/ace-11.0.0.3/ace accept license silently && echo "source /opt/IBM/ace-11.0.0.3/server/bin/mqsiprofile" >> /home/aceadmin/.bashrc && echo  "ODBCINI=/opt/IBM/ace-11.0.0.3/server/ODBC/unixodbc/odbc.ini;" >> /home/aceadmin/.bash_profile && echo "export ODBCINI"  >> /home/aceadmin/.bash_profile && echo  "ODBCSYSINI=/opt/IBM/ace-11.0.0.3/server/ODBC/unixodbc/odbcinst.ini;" >> /home/aceadmin/.bash_profile && echo "export ODBCSYSINI"  >> /home/aceadmin/.bash_profile
RUN mkdir /opt/IBM/ace-11.0.0.3/server/workdir && chmod -R 775 /opt/IBM/ace-11.0.0.3/server/workdir && chown -R aceadmin:mqbrkrs /opt/IBM/ace-11.0.0.3/server/workdir
RUN  . /opt/IBM/ace-11.0.0.3/server/bin/mqsiprofile && mqsicreateworkdir /opt/IBM/ace-11.0.0.3/server/workdir/core
USER root
COPY server.conf.yaml /opt/IBM/ace-11.0.0.3/server/workdir/core
RUN chown -R aceadmin:mqbrkrs /opt/IBM/ace-11.0.0.3/server/workdir/core  && chmod -R 777 /opt/IBM/ace-11.0.0.3/server/workdir/core
USER aceadmin
EXPOSE 7600 7800 7843
CMD ["./ini.sh"]
